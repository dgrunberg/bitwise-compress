# Bitwise Compress

Study compression of files by bytes vs. bitwise
See post at www.solverworld.com/bitwise-compression-what-is-it

Run study-bits.py as the main program.  Use --help to see command line options.
You might want to use enwik8 from prize.hutter1.net as a test file as in the blog post.

study-bits.py    main program
sutil.py         utility functions module
arith.py         Arithmetic Coder module
