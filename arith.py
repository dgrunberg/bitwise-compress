#!/usr/bin/env python
#Copyright (C) 2019 Daniel B Grunberg
#Governed by GNU Affero General Public License
#See LICENSE file for details

import re

'''
Arithmetic Coding module in python
Probabilities are given as integer/MAX.  Example, for MAX=4096, prob=1011 gives p=0.2468...
BitsIn and BitsOut allow files to be dealt with 1 bit at a time, and are used for testing
the Arith class.
'''
#For probabilities
SHIFT=12
MAX=2**SHIFT
#For low/high values
BITS_FOR_CODE=32
TOP=(1<<BITS_FOR_CODE)-1
FIRST_QUARTER=int(TOP/4)+1
HALF=2*FIRST_QUARTER
THIRD_QUARTER=3*FIRST_QUARTER
#print('{:08X} {:08X} {:08X}'.format(FIRST_QUARTER, HALF, THIRD_QUARTER))

def show2(s,a,b):
  print('{:10} {:08X}:{:08X}  {:10}:{:10}'.format(s,a,b,a,b))

#The Bit classes will use high order bit first (reading or writing)  
class BitsIn():
  #Class to open a file and retrieve 1 bit at a time
  #Closes file when last reference goes away; can call close earlier
  #Should we use a context instead to manage file closing instead?
  def __init__(self,filename):
    self.filename=filename
    self.fp=open(filename,'rb')
    self.bits=0   #number of bits read from current byte
    #can't read yet because we might do a header
    self.byte=None
    self.total_bits=0

  def get_bytes(self,n):
    #read n bytes from the file (presumably a header).  Need total_bits==0
    assert self.total_bits==0
    return self.fp.read(n)
  
  def get(self):
    #returns 0 or 1 (high order bit first)
    if self.byte is None:
      self.byte=self.fp.read(1)[0]   #current byte, rotated so HO bit is next to read
    if self.bits==8:
      b=self.fp.read(1)
      if len(b)==0:
        raise EOFError
      self.byte=b[0]
      self.bits=0
    x=1 if self.byte & 0x80 else 0
    self.byte=self.byte<<1
    self.bits+=1
    self.total_bits+=1
    return x
    
  def close(self):
    self.fp.close()
    self.fp=None

  def __del__(self):
    if self.fp is not None:
      self.close()

class BitsOut():
  #Class to open a file and write 1 bit at a time to it
  #Closes file when last reference goes away; can call close earlier
  def __init__(self,filename):
    self.filename=filename
    self.fp=open(filename,'wb')
    self.byte=0    #byte so far
    self.bits=0    #bits in this byte so far
    self.total_bits=0

  def out_bytes(self,b):
    #write the byte string b to the file - must do this first thing so as not to disturb bit stream
    #These bytes are not counted in total_bits, should be used for a header
    assert self.total_bits==0
    self.fp.write(b)
    
  def out(self,x):
    #x should be a 0, 1, or a string of '0', '1', ' '.  The spaces will be ignored
    if x==0:
      #print('outputting #{} {}'.format(self.total_bits,x))
      self.byte= self.byte<<1
      self.bits+=1
      self.total_bits+=1
    elif x==1:
      #print('outputting #{} {}'.format(self.total_bits,x))
      self.byte= (self.byte<<1)+x
      self.bits+=1
      self.total_bits+=1
    elif isinstance(x, str):
      for s in x:
        try:
          if s==' ':
            pass
          elif int(s)==0 or int(s)==1:
            self.out(int(s))
        except ValueError:
          raise('BitsOut:out, bad element in string {}'.format(s))
    if self.bits==8:
      self.fp.write(bytes([self.byte]))
      self.bits=0
      self.byte=0

  def close(self):
    #write out the remaining bits if there are any, then close file
    if self.bits>0:
      self.byte=self.byte<<(8-self.bits)
      self.fp.write(bytes([self.byte]))
    self.fp.close()
    self.fp=None

  def __del__(self):
    if self.fp is not None:
      self.close()

class ArithEncoder():
  def __init__(self, outbit_fcn=None,debug=False):
    if outbit_fcn is None:
      self.outbit_fcn=self.output
    else:
      self.outbit_fcn=outbit_fcn
    self.high=TOP
    self.low=0
    self.pending_bits=0
    self.bits_out=0
    self.bits_in=0
    self.bits_to_follow=0  # number of opposite bits after the next bit
    self.debug=debug

  def output(self,x):
    if self.debug:
      print('bit out {}'.format(x))
    self.bits_out+=1
    
  def bit_plus_follow(self,x):
    assert x in [0,1]
    self.outbit_fcn(x)
    if self.debug:
      print('output #{} {}'.format(self.bits_out,x))
    self.bits_out+=1
    for i in range(self.bits_to_follow):
      self.outbit_fcn(1-x)
      if self.debug:
        print('output #{} {}'.format(self.bits_out,x))
      self.bits_out+=1
    self.bits_to_follow=0
  
  def encode(self,value, prob):
    #Encode a bit where prob of bit==1 is given by prob/MAX
    assert value in (0,1)
    if self.debug:
      print('\n#{} encode bit {} prob {}'.format(self.bits_in,value,prob))
    if prob<=0:
      prob=1
    if prob>=MAX:
      prob=MAX-1  
    self.bits_in+=1
    #encode a value (0 or 1), where prob is probability of a 1 (determined by model)
    #p=prob/X.MAX
    if self.debug:
      show2('Enter',self.low,self.high)
    range=self.high-self.low+1
    assert range>0
    if value==1:
      plower=0
      pupper=prob
    else:
      plower=prob
      pupper=MAX
    self.high = self.low + (int(range*pupper)>>SHIFT)-1
    if self.debug:
      print('range {} plower={} add {}'.format(range,plower,int(range*plower)>>SHIFT))
    self.low = self.low + (int(range*plower)>>SHIFT)
    if self.debug:
      show2('After',self.low,self.high)
    while True:
      if self.high < HALF:
        self.bit_plus_follow(0)
      elif self.low >= HALF:
        self.bit_plus_follow(1)
        self.low-=HALF     # removes the highest 1 bit
        self.high-=HALF    # removes the highest 1 bit
      elif self.low>=FIRST_QUARTER and self.high<THIRD_QUARTER:
        self.bits_to_follow+=1
        self.low-=FIRST_QUARTER
        self.high-=FIRST_QUARTER
      else:
        break
      self.low=2*self.low
      self.high=2*self.high+1
      if self.debug:
        show2('Shift',self.low,self.high)
      
  def done_encoding(self):
    #call after last symbol to make sure last stuff goes out
    if self.debug:
      print('done_encoding')
    self.bits_to_follow+=1
    if self.low<FIRST_QUARTER:
      self.bit_plus_follow(0)
    else:
      self.bit_plus_follow(1)

class ArithDecoder():
  def __init__(self, total_bits,get_bit,debug=False):
    #Needs total bits (of original stream) to know when done, unless we use a terminator symbol
    self.get_bit=get_bit
    self.high=TOP
    self.low=0
    self.value=0
    self.total_bits=total_bits
    self.bits_out=0
    self.debug=debug
    #prestuff the code value
    for i in range(BITS_FOR_CODE):
      self.value=2*self.value+self.get_bit()
    if self.debug:
      print('starting value {:08X}'.format(self.value))

  def decode_symbol(self,prob):
    if prob<=0:
      prob=1
    if prob>=MAX:
      prob=MAX-1  
    if self.debug:
      print('In decode symbol, prob={}'.format(prob))
    #decode a symbol (0 or 1), where prob is probability of a 1 (determined by model)
    #p=prob/X.MAX
    if self.debug:
      show2('Enter',self.low,self.high)
    range=self.high-self.low+1
    assert range>0
    if self.debug:
      print('value 0x{:08X} {}'.format(self.value,self.value))
    #Are we in the symbol 1 range or symbol 0 range?
    #NOTE: This should be converted to a integer compare, but float OK for now
    if (self.value-self.low)/range < prob/MAX:
      s=1
    else:
      s=0
    if self.debug:
      print('{} vs {}'.format((self.value-self.low)/range,prob/MAX))
    if s==1:
      plower=0
      pupper=prob
    else:
      plower=prob   #? not sure if we need +1 here
      pupper=MAX
    if self.debug:
      print('#{} got symbol {}'.format(self.bits_out,s))
    self.bits_out+=1
    self.high = self.low + (int(range*pupper)>>SHIFT)-1
    self.low = self.low + (int(range*plower)>>SHIFT)
    if self.debug:
      show2('After',self.low,self.high)
    while True:
      if self.high < HALF:
        pass
      elif self.low >= HALF:
        self.low-=HALF     # removes the highest 1 bit
        self.high-=HALF    # removes the highest 1 bit
        self.value-=HALF
      elif self.low>=FIRST_QUARTER and self.high<THIRD_QUARTER:
        self.low-=FIRST_QUARTER
        self.high-=FIRST_QUARTER
        self.value-=FIRST_QUARTER
      else:
        break
      self.low=2*self.low
      self.high=2*self.high+1
      try:
        b=self.get_bit()
      except EOFError:
        #we need to read more bits than in the file at the end, so assume 0
        b=0
      self.value=2*self.value+b
      if self.debug:
        show2('Loop',self.low,self.high)
    return s
      
if __name__=='__main__':
  #Original string.  spaces are for readability, we will strip out before compression
  sorigx='00010 10000 00100 00010 00010 10000 00100 00001 10000 01000 10000 10000'
  #sorig='00010 10000 00100 00010 00010 10000 00100 00001'
  sorig=re.sub(' ','',sorigx)
  #sorig=sorig*12
  print('orig string ({})={}'.format(len(sorig),sorig))
  writeobj=BitsOut('test.bin')
  writeobj.out(sorigx)
  writeobj.close()
  bits_orig=len(sorig)
  print('bits orig {}'.format(bits_orig))
  #read from the file and compare
  readobj=BitsIn('test.bin')
  for i in range(bits_orig):
    if readobj.get() != int(sorig[i]):
      print('mismatch at {} in reading orig bit file'.format(i))
      sys.exit(1)
  print('read matches')
    
  #Now compress this with encoder into 'test-comp.bin'
  readobj=BitsIn('test.bin')
  writeobj=BitsOut('test-comp.bin')
  x=ArithEncoder(outbit_fcn=writeobj.out)
  bits=0
  p0=int(0.8*MAX)
  p1=int(0.2*MAX)
  for i in range(bits_orig):
    c=readobj.get()
    #print('read #{} {}'.format(bits,c))
    x.encode(c, p1)
    bits+=1
  x.done_encoding()
  readobj.close()
  writeobj.close()
  print('bits in {} out {} ratio {}'.format(bits,x.bits_out, x.bits_out/bits))
  #print('output compressed stream ({})={}'.format(len(string),string))
  #now decode
  print('Start decoding file')
  readobj=BitsIn('test-comp.bin')
  writeobj=BitsOut('test-reconstruct.bin')

  index=0
  d=ArithDecoder(bits_orig,readobj.get)
  s=''
  while True:
    q=d.decode_symbol(p1)
    writeobj.out(q)
    if q==0:
      s=s+'0'
    else:
      s=s+'1'
    if len(s)==d.total_bits:
      break
  print('decoder output ({}) {}'.format(len(s),s))
  readobj.close()
  writeobj.close()
  if s==sorig:
    print('output matches original')
  else:
    for i in range(len(s)):
      if s[i]!=sorig[i]:
        print('does not match at index {}'.format(i))
        break
    
    
    
    
    
