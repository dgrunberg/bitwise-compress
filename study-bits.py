#!/usr/bin/env python
#Copyright (C) 2019 Daniel B Grunberg
#Governed by GNU Affero General Public License
#See LICENSE file for details
import collections
import math
import numpy as np
import sys
import argparse
import time
import os
import signal
import datetime
### Local Modules
import sutil as util
import arith
'''
Simple compressor to get the statistics of bit-wise compression and compare to 
byte-wise.  To do this, we compute the bitcontext statistics on the first
pass, then go back and compute compression with those fixed stats
'''
quit=False
assert sys.version_info >= (3,5)

def signal_handler(sig, frame):
  global quit
  print('pressed Ctrl+C')
  if quit:
    #really exit on 2nd press
    sys.exit(1) 
  quit=True
signal.signal(signal.SIGINT, signal_handler)

#This just keeps track of the count of number of occurrences
#state can be a tuple or just 0 if we do not want to use any state
class Context():
  #Context of an integer (could be position of this word so far
  def __init__(self, log=None,def_prob=0.5):
    self.name='Context'
    #we track the total number of occurrences and the number of times we have see
    # a 1
    #state is compose of n previous symbols, a symbol, and number of HO bits to
    #use from that single byte
    #whether to collect the stats when adding symbols
    self.def_prob=def_prob
    self.reset()
                       
  def reset(self):
    self.total=collections.defaultdict(int)
    self.ones=collections.defaultdict(int)
    self.current_state=(0,)

  def predict(self):
    if self.total[self.current_state] > 0:
      pr=self.ones[self.current_state]/self.total[self.current_state]
    else:
      pr=self.def_prob
    return pr

  def observed_bit(self,y):
    #just saw a bit, update stuff
    self.total[self.current_state]+=1
    if y:
      self.ones[self.current_state]+=1

  def update_state(self,bitcontext,state):
    #set the state to be bitcontext plus whatever other state we want to use
    state=(state,bitcontext)
    self.current_state=state

parser = argparse.ArgumentParser(description='Run compression (enwik8->preproc->compress)')
parser.add_argument('--input', type=str,help='preproc file name',default='enwik8')
parser.add_argument('--meg', type=float,help='number of 1,000,000 bytes to compress,default is entire file',default=None)
parser.add_argument('--output', type=str,help='output file for compressed file',default='output.bin')
parser.add_argument('--context', type=int,help='number of previous bytes for context',default=0)
options=parser.parse_args()

now=datetime.datetime.now()

print('Starting  {}'.format(now.isoformat()))
print('Args: {}'.format(' '.join(sys.argv)))

def compress(contexts,debug=False,update=True,reset=True):
  #compress list of words
  #configuration options
  use_weights=False   # to use learned weights and more than 1 model
  use_priors=False
  #keep track of symbol counts so we can do a entropy calculation
  symbol_counts=collections.defaultdict(int)
  with open(options.input, 'rb') as fp:
    data=fp.read()
  input_bytes=len(data)
  print('read in dictionary {}'.format(options.input))
  if options.meg is not None:
    data=data[0:int(options.meg*1e6)]
    input_bytes=len(data)
  print('size of data is {}'.format(len(data)))
  writeobj=arith.BitsOut(options.output)
  ac=arith.ArithEncoder(outbit_fcn=writeobj.out,debug=debug)
  total_bits=0
  W=len(contexts)
  if reset:
    #reset state before starting
    for c in contexts:
      c.reset()
  start_time2=time.time()
  #use adaptive weights, start with equal weights
  bits_out=0
  pr=np.zeros((W,))  # gets overwritten each time anyway
  #set the weights to zero on the first model unless ws==0
  for w,c in enumerate(data):
    if w%50000==0:
      print('byte {:6}'.format(w))
    symbol_counts[c]+=1
    #compute the two previous characters (before the replacement occurred)
    for bitnum in range(8):
      #the bit we are encoding.  0 means no prior bits (just the full bytes)
      #0=HO bit, no prior bits being used, just full bytes
      #we want highest order bit first
      bitcontext=util.bit_context8(c,bitnum)   #all bits seen so far
      for ii,context in enumerate(contexts):
        if options.context==0 or w==0:
          state=(0,)  # not using state right now
        else:
          #use a sequence of bytes ending just before the current one
          state=(data[w-options.context:w],)
        context.update_state(bitcontext, state)
      y=(c >> (7-bitnum)) & 1
      for i,context in enumerate(contexts):
        pr[i]=context.predict()
      mixed_prob=pr[0]
      if mixed_prob<util.eps:
        mixed_prob=util.eps
      elif mixed_prob>util.mx:
        mixed_prob=util.mx
      prt=int(mixed_prob*arith.MAX)
      ac.encode(y, prt)
      if y==1:
        bs=util.bits(mixed_prob)
      else:
        bs=util.bits(1.0-mixed_prob)
      bits_out+=bs
      #Now update all the contexts with the new bit seen
      if update:
        for context in contexts:
          #update the counts from seeing y
          context.observed_bit(y)
      #end of bits
    #end of byte
  ac.done_encoding()
  writeobj.close()
  print('Estimated compression bits out {:8.2f} ratio {:8.4f}'.format(bits_out,bits_out/8/input_bytes))
  print('Actual bits out in encoder     {:8.2f} ratio {:8.4f}'.format(writeobj.total_bits,
                                                      writeobj.total_bits/8/input_bytes))
  total=sum(symbol_counts.values())
  entropy=sum([-(v/total)*util.log2(v/total) for v in symbol_counts.values() ])
  print('Entropy bits per byte (symbol) {:8.4f} ratio {:8.4f}'.format(entropy,entropy/8*total/input_bytes))
  
contexts=[Context()]
compress(contexts,reset=True,update=True)
compress(contexts,reset=False,update=False)
  
print('Done.')
