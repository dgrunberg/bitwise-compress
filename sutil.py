#!/usr/bin/env python
#Copyright (C) 2019 Daniel B Grunberg
#Governed by GNU Affero General Public License
#See LICENSE file for details

import math
import numpy as np
import time

#for Logistic mixing (on arrays)
#These are max and min values for log, so no errors on 0 or 1 probs
eps=1e-6
mx=1-eps
neg=np.log(eps/(1-eps))
pos=np.log(mx/(1-mx))

def stretch(x):
  #take care of very small or very large values
  if isinstance(x,float):
    if x<eps:
      return neg
    elif x>mx:
      return pos
    else:
      return np.log(x/(1-x))
  mask1=x<eps
  mask2=x>mx
  ret=np.log(x/(1-x))
  ret[mask1]=neg
  ret[mask2]=pos
  return ret

def squash(x):
  return 1/(1+np.exp(-x))

def log2(x):
  return np.log2(x)

def bits(prob):
  #bits output for a given probability in AC
  return -np.log2(prob)

def bit_y(symbol, bitnum):
  #get a bit (bitnum'th) of symbol, where 0 is the first bit of a 16 bit number
  return (symbol >> (15-bitnum)) & 1

def bit_context(sym, bit):
  #convert to a context that can used to lookup the first bit bits of sym
  #Puts a '1' in front of the relevant bits. 1 is the return for no context (bit 0)
  return sym>>(16-bit) | (0x8000 >> (15-bit))

def bit_context8(sym, bit):
  #convert to a context that can used to lookup the first bit bits of sym
  #Puts a '1' in front of the relevant bits. 1 is the return for no context (bit 0)
  #same as bit_context() but only 8 bit context
  return (sym>>(8-bit)) | (0x80 >> (7-bit))

import subprocess
def get_git_commit():
    #return latest git commit hash
    #NOTE: could use: git show -s HEAD --format="%h %ci" for commit and datetime
    x=['git','show', '-s', 'HEAD', '--format="%h %ci"']
    #x=['git','rev-parse','--short','master']
    result=subprocess.run(x, stdout=subprocess.PIPE)
    return result.stdout.decode('utf-8')

  
start_time=time.time()
def dprint(*args,**kwargs):
  ti='{:8.3f} '.format(time.time()-start_time)
  print(ti, *args, **kwargs)

if __name__=='__main__':
  p=.78
  p1=stretch(p)
  p2=squash(p1)
  print(p2-p)
  #test on arrays
  p=np.array([0,0.01,.25,.502,.81, .99,1])
  p1=stretch(p)
  p2=squash(p1)
  print(p)
  print(p1)
  print(p2)
  err=np.linalg.norm(p-p2)
  assert err < 1e-7
  print('OK')
  
  
